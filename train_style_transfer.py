import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision.transforms as transforms
import torchvision.models as models

import numpy as np

from PIL import Image

import matplotlib.pyplot as plt

import copy

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

loader = transforms.Compose([
    transforms.Resize(512),  # scale imported image
    transforms.CenterCrop((512,512)),  # scale imported image
    transforms.ToTensor()])  # transform it into a torch tensor

def image_loader(image_name):
    image = Image.open(image_name)
    # fake batch dimension required to fit network's input dimensions
    image = loader(image).unsqueeze(0)
    return image.to(device, torch.float)

# create a module to normalize input image so we can easily put it in a
# nn.Sequential
class Normalization(nn.Module):
    def __init__(self, mean, std):
        super(Normalization, self).__init__()
        # .view the mean and std to make them [C x 1 x 1] so that they can
        # directly work with image Tensor of shape [B x C x H x W].
        # B is batch size. C is number of channels. H is height and W is width.
        self.mean = torch.tensor(mean).view(-1, 1, 1)
        self.std = torch.tensor(std).view(-1, 1, 1)

    def forward(self, img):
        # normalize img
        return (img - self.mean) / self.std

class CnnStyleLoss(nn.Module):
    def __init__(self):
        super(CnnStyleLoss, self).__init__()

        self.cnn = models.vgg19(pretrained = True).features.to(device).eval()

        cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406]).to(device)
        cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225]).to(device)
        self.normalization = Normalization(cnn_normalization_mean,
                                            cnn_normalization_std).to(device)
    
    def get_conv_layer_index(self):
        index_conv = []
        for i,layer in enumerate(cnn.children()):
            if isinstance(layer,nn.Conv2d):
                index_conv.append(i)
        return index_conv

    @staticmethod
    def gram_matrix(input):
        a, b, c, d = input.size() 
        # a=batch size(=1)
        # b=number of feature maps
        # (c,d)=dimensions of a f. map (N=c*d)

        features = input.view(a, b, c * d)  # resise F_XL into \hat F_XL

        # apply on all element of the batch
        N = b * c * d
        G = torch.stack([
            torch.mm(f, f.t()).div(N) for f in torch.unbind(features, dim=0)
        ], dim=0)

        return G
    
    @staticmethod
    def gram_content_from_sequential(seq_model, x, gram_layer = None, content_layer = None):
        # included_layer = None => all layer taken into account

        i, gram_computed, content_computed = 0, 0, 0
        gram_matrices, content_matrices = [], []
        
        tmp = x
        for layer in seq_model.children():
            if isinstance(layer, nn.ReLU):
                # The in-place version doesn't play very nicely with the ContentLoss
                # and StyleLoss we insert below. So we replace with out-of-place
                # ones here.
                layer = nn.ReLU(inplace=False)
            
            tmp = layer(tmp) # forward pass
            
            # if convolutional layer
            if isinstance(layer,nn.Conv2d):
                # needed gram matrix
                if (gram_layer is not None) and (i in gram_layer):
                    gram_matrices.append(CnnStyleLoss.gram_matrix(tmp))
                    gram_computed += 1
                # needed content matrix
                if (content_layer is not None) and (i in content_layer):
                    content_matrices.append(tmp)#.clone()
                    content_computed += 1
                
                # early stop
                style_stop = (gram_layer is None) or (gram_computed >= len(gram_layer))
                content_stop = (content_layer is None) or (content_computed >= len(content_layer))
                if style_stop and content_stop:
                    break
                # update conv index
                i += 1
        
        return gram_matrices, content_matrices

    def forward(self, x, target, style, gram_layer = None, content_layer = None):

        with torch.no_grad():
            norm_target = self.normalization(target)
            _,target_content = self.gram_content_from_sequential(self.cnn,
                                                                norm_target,
                                                                gram_layer = None,
                                                                content_layer = content_layer)

            norm_style = self.normalization(style)
            style_gram,_ = self.gram_content_from_sequential(self.cnn,
                                                            norm_style,
                                                            gram_layer = gram_layer,
                                                            content_layer = None)

        norm_x = self.normalization(x)
        x_gram,x_content = self.gram_content_from_sequential(self.cnn,
                                                            norm_x,
                                                            gram_layer = gram_layer,
                                                            content_layer = content_layer)

        style_losses = [F.mse_loss(x_f, style_f) for x_f,style_f in zip(x_gram,style_gram)]
        style_loss = sum(style_losses)

        content_losses = [F.mse_loss(x_c, target_c) for x_c,target_c in zip(x_content,target_content)]
        content_loss = sum(content_losses)
        
        return style_loss, content_loss

def get_input_optimizer(input_img):
    # this line to show that input is a parameter that requires a gradient
    optimizer = optim.LBFGS([input_img.requires_grad_()])
    return optimizer

def run_style_transfer(content_img, style_img, input_img, num_steps=300,
                       style_weight=1000000, content_weight=1):
    """Run the style transfer."""
    print('Building the style transfer model..')
    ccnStyleLoss = CnnStyleLoss()
    optimizer = get_input_optimizer(input_img)

    print('Optimizing..')
    run = [0]
    while run[0] <= num_steps:

        def closure():
            # correct the values of updated input image
            input_img.data.clamp_(0, 1)

            optimizer.zero_grad()
            #print('input grad?',input_img.requires_grad)

            style_score, content_score = ccnStyleLoss(input_img, content_img, style_img,
                                                        gram_layer = [0,1,2,3,4], content_layer = [3])
            
            style_score *= style_weight
            content_score *= content_weight
            
            loss = style_score + content_score

            loss.backward()

            run[0] += 1
            if run[0] % 50 == 0:
                print("run {}:".format(run))
                print('Style Loss : {:4f} Content Loss: {:4f}'.format(
                    style_score.item(), content_score.item()))
                print()

            return style_score + content_score

        optimizer.step(closure)

    # a last correction...
    input_img.data.clamp_(0, 1)

    return input_img

if __name__ == '__main__':
    print('run on: {}'.format(device))

    style_img = image_loader("./files/style.jpg")
    content_img = image_loader("./files/base.jpg")
    
    input_img = content_img.clone()

    output = run_style_transfer(content_img, style_img, input_img)

    plt.subplot(131)
    plt.title('base image')
    plt.axis('off')
    plt.imshow(np.transpose(content_img[0].cpu().clone(),(1,2,0)))
    plt.subplot(132)
    plt.title('style image')
    plt.axis('off')
    plt.imshow(np.transpose(style_img[0].cpu().clone(),(1,2,0)))
    plt.subplot(133)
    plt.title('output image')
    plt.axis('off')
    plt.imshow(np.transpose(output[0].cpu().clone().detach().numpy(),(1,2,0)))
    plt.show()
    
