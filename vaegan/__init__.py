import torch
import torch.nn as nn
from torch.autograd import Variable

from .encoder import Encoder
from .decoder import Decoder
from .discriminator import Discriminator

class VAEGAN(nn.Module):
    def __init__(self, input_size = (64,64), latent_dim = 128, momentum = 0.1, eps = 1e-5, residual_blocks = 16):
        super(VAEGAN,self).__init__()

        self.latent_dim = latent_dim
        self.input_size = input_size

        self.encoder = Encoder(input_size = input_size, latent_dim = latent_dim, momentum = momentum, eps = eps)
        self.decoder = Decoder(input_size = input_size, latent_dim = latent_dim, residual_blocks = residual_blocks)
        self.discriminator = Discriminator(input_size = input_size)

        '''
        self.init_kaiming_uniform(self.encoder)
        self.init_kaiming_uniform(self.decoder)
        self.init_kaiming_uniform(self.discriminator)
        '''

        self.normal_distribution = torch.distributions.normal.Normal(0,1)

        self.adv_loss = nn.BCELoss()
        self.mse_loss = nn.MSELoss()
    
    @staticmethod
    def init_kaiming_uniform(seq_model):
        for layer in seq_model.children():
            if type(layer)==nn.Conv2d or type(layer)==nn.Linear:
                nn.init.kaiming_uniform_(layer.weight.data)

    def enc_parameters(self):
        p_enc = self.encoder.parameters()
        p_logvar = self.fc_logvar.parameters()
        p_mean = self.fc_mean.parameters()
        return list(p_enc) + list(p_logvar) + list(p_mean)

    def dec_parameters(self):
        return self.decoder.parameters()

    def dis_parameters(self):
        return self.discriminator.parameters()

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std
    
    def forward(self,x):
        enc = self.encoder(x)
        mean = self.fc_mean(enc)
        logvar = self.fc_logvar(enc)
        z = self.reparameterize(mean, logvar)
        x_tilde = self.decoder(z)
        return x_tilde
    
    def loss_function(self, x, beta, gamma, device = 'cuda'):
        batch_size = x.size()[0]
        w, h = self.input_size

        valid = Variable(torch.ones((batch_size,1)), requires_grad=False).to(device)
        fake = Variable(torch.zeros((batch_size,1)), requires_grad=False).to(device)
        
        # prop
        self.encoder.train()
        self.decoder.train()
        self.discriminator.train()
        z, mean, logvar = self.encoder(x)
        l_prior = -0.5 * torch.mean(1 + logvar - mean.pow(2) - logvar.exp())

        x_tilde = self.decoder(z)
        l_content = self.mse_loss(x_tilde, x)

        z_p = self.normal_distribution.sample((batch_size,self.latent_dim)).to(device)
        x_p = self.decoder(z_p)

        self.discriminator.eval()
        dis_x_tilde = self.discriminator(x_tilde)
        dis_x_p = self.discriminator(x_p)
        l_gan_dec = 1.0/2.0*(self.adv_loss(dis_x_tilde, valid) + self.adv_loss(dis_x_p, valid))

        self.discriminator.train()
        dis_x = self.discriminator(x)
        dis_x_tilde = self.discriminator(x_tilde.detach().clone())
        dis_x_p = self.discriminator(x_p.detach().clone())
        l_gan_dis = 1.0/3.0*(self.adv_loss(dis_x, valid) + self.adv_loss(dis_x_tilde, fake) + self.adv_loss(dis_x_p, fake))

        l_enc = l_content + beta*l_prior
        l_dec = l_content + gamma*l_gan_dec
        l_dis = l_gan_dis
        
        return l_enc, l_dec, l_dis, l_prior, l_content, l_gan_dis, l_gan_dec

