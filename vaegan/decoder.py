import torch
import torch.nn as nn

from .utils import Reshape, ResidualBlock
        
class Decoder(nn.Module):
    def __init__(self, input_size = (64,64), latent_dim = 128, residual_blocks = 16):
        super(Decoder,self).__init__()

        self.input_size = input_size
        self.latent_dim = latent_dim

        w, h = self.input_size
        dec_in_size = (w//8)*(h//8)*64

        self.pre_residual = nn.Sequential(
            nn.Linear(latent_dim, dec_in_size),
            Reshape((64, w//8, h//8))
        )

        res_blocks = [ResidualBlock() for _ in range(residual_blocks)]
        self.residual_blocks = nn.Sequential(*res_blocks)

        self.upsampling = nn.Sequential(            
            nn.Conv2d(64, 256, 3, padding = 1),
            nn.PixelShuffle(2),
            nn.PReLU(),
            
            nn.Conv2d(64, 256, 3, padding = 1),
            nn.PixelShuffle(2),
            nn.PReLU(),
            
            nn.Conv2d(64, 256, 3, padding = 1),
            nn.PixelShuffle(2),
            nn.PReLU(),

            nn.Conv2d(64, 3, 9, padding = 4),
            nn.Tanh()
        )
    
    def forward(self, z):
        pre_residual = self.pre_residual(z)
        post_residual = self.residual_blocks(pre_residual)
        x_tilde = self.upsampling(pre_residual + post_residual)
        return x_tilde
