import torch
import torch.nn as nn

from .utils import DisplayShape

class Encoder(nn.Module):
    def __init__(self, input_size = (64,64), latent_dim = 128, momentum = 0.1, eps = 1e-5):
        super(Encoder,self).__init__()
        
        self.input_size = input_size
        self.latent_dim = latent_dim

        w, h = self.input_size
        enc_out_size = (w//8)*(h//8)*256

        self.conv = nn.Sequential(
            nn.Conv2d(3, 64, 5, padding = (2,2), stride = 2),
            nn.BatchNorm2d(64, momentum = momentum, eps = eps),
            nn.LeakyReLU(0.2),
            nn.Conv2d(64, 128, 5, padding = (2,2), stride = 2),
            nn.BatchNorm2d(128, momentum = momentum, eps = eps),
            nn.LeakyReLU(0.2),
            nn.Conv2d(128, 256, 5, padding = (2,2), stride = 2),
            nn.BatchNorm2d(256, momentum = momentum, eps = eps),
            nn.LeakyReLU(0.2),
            nn.Flatten(),
        )

        self.fc_mean = nn.Linear(enc_out_size, latent_dim)
        self.fc_logvar = nn.Linear(enc_out_size, latent_dim)
        
    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std

    def forward(self, x):
        enc = self.conv(x)

        mean = self.fc_mean(enc)
        logvar = self.fc_logvar(enc)
        z = self.reparameterize(mean, logvar)

        return z, mean, logvar
