import torch
import torch.nn as nn

class Reshape(nn.Module):
    def __init__(self, shape):
        super(Reshape, self).__init__()
        self.shape = shape

    def forward(self, x):
        return x.view(-1,*self.shape)

class DisplayShape(nn.Module):
    def __init__(self):
        super(DisplayShape, self).__init__()

    def forward(self, x):
        print(x.size())
        return x

class ResidualBlock(nn.Module):
    def __init__(self, filter_count : int = 64):
        super(ResidualBlock,self).__init__()

        self.convolutions = nn.Sequential(
            nn.Conv2d(filter_count, filter_count, 3, padding = 1),
            nn.BatchNorm2d(filter_count),
            nn.PReLU(),
            nn.Conv2d(filter_count, filter_count, 3, padding = 1),
            nn.BatchNorm2d(filter_count),
        )

    def forward(self,x):
        return self.convolutions(x) + x