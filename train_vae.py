from model import VAE

import numpy as np
import matplotlib.pyplot as plt

import time
import os
import sys

import torch
import torch.optim as optim
import torch.nn.functional as F

def train(device, vae, optimizer, train_loader, epoch,
            beta, log_interval = 10, latent_dim = 128, input_size = (128,128),
            style_transfer = False, early_stop = None,
            save_state_dir = './results/vae', verbose = True):
    try:
        os.makedirs(save_state_dir)
    except OSError:
        pass

    train_vae_losses = []
    vae.to(device).eval()
    vae.train()
    t0 = time.time()
    for batch_idx, (data, _) in enumerate(train_loader):
        optimizer.zero_grad()
        data = data.to(device)
        output, mean, logvar = vae(data)
        if style_transfer:
            loss, style_loss, content_loss, kld = vae.loss_function_with_transfer(data, output, mean, logvar, beta)
        else:
            loss, bce, kld = vae.loss_function(data, output, mean, logvar, beta)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            if verbose:
                if style_transfer:
                    print('Train Epoch: {} [{:>5}/{} ({:>2.0f}%)]\tLoss: {:2.6f}, style: {:2.4f}, content: {:2.4f}, kld: {:2.4f} (in {:.2f}s)'.format(
                            epoch, batch_idx * len(data), len(train_loader.dataset),
                            100. * batch_idx / len(train_loader), loss.item(),
                            style_loss.item(), content_loss.item(), kld.item(),time.time()-t0))
                else:
                    print('Train Epoch: {} [{:>5}/{} ({:>2.0f}%)]\tLoss: {:2.6f}, bcd: {:2.4f}, kld: {:2.4f} (in {:.2f}s)'.format(
                            epoch, batch_idx * len(data), len(train_loader.dataset),
                            100. * batch_idx / len(train_loader), loss.item(), bce.item(), kld.item(),time.time()-t0))
            train_vae_losses.append(loss.item())
            torch.save(vae.state_dict(), os.path.join(save_state_dir,'model.pth'))
            torch.save(optimizer.state_dict(), os.path.join(save_state_dir,'optimizer.pth'))
            t0 = time.time()
            if early_stop is not None:
                early_stop.add_batch_loss(loss)
                if early_stop.stop_condition():
                    break
            sys.stdout.flush()
    if verbose:
        print('') # new line
    sys.stdout.flush()
    return train_vae_losses

def load(device, latent_dim = 128, input_size = (128,128), directory = './results/vae'):
    model = VAE(latent_dim = latent_dim, input_size = input_size)
    model.load_state_dict(torch.load(os.path.join(directory,'model.pth')), strict=False)
    return model.to(device).eval()

def test(device, vae,test_loader,beta,
            style_transfer = False):
    test_vae_losses = []
    test_kld_losses = []
    if style_transfer:
        test_content_losses = []
        test_style_losses = []
    else:
        test_bce_losses = []
    vae.to(device).eval()
    with torch.no_grad():
        for data, _ in test_loader:
            data = data.to(device)
            output, mean, logvar = vae(data)
            if style_transfer:
                test_loss, style_loss, content_loss, kld = vae.loss_function_with_transfer(data, output, mean, logvar, beta)
                test_loss, kld = test_loss.cpu().clone(), kld.cpu().clone()
                style_loss, content_loss = style_loss.cpu().clone(), content_loss.cpu().clone()
            else:
                test_loss, bce, kld = vae.loss_function(data, output, mean, logvar, beta)
                test_loss, bce, kld = test_loss.cpu().clone(), bce.cpu().clone(), kld.cpu().clone()
            test_vae_losses.append(test_loss.item())
            test_kld_losses.append(kld.item())
            if style_transfer:
                test_style_losses.append(style_loss.item())
                test_content_losses.append(content_loss.item())
            else:
                test_bce_losses.append(bce.item())
        if style_transfer:
            print('Test set: Avg. loss: {:.4f}, style: {:.4f}, content: {:.4f}, kld: {:.4f}'.format(np.mean(test_vae_losses),
                                                                                                np.mean(test_style_losses),
                                                                                                np.mean(test_content_losses),
                                                                                                np.mean(test_kld_losses)))
        else:
            print('Test set: Avg. loss: {:.4f}, bce: {:.4f}, kld: {:.4f}'.format(np.mean(test_vae_losses),
                                                                                np.mean(test_bce_losses),
                                                                                np.mean(test_kld_losses)))
        sys.stdout.flush()
    return np.mean(test_vae_losses)

def generate_sample(device, vae, size = 1):
    import numpy as np

    w, h = vae.input_size

    vae.to(device).eval()
    samples = np.empty((size,w,h,3))

    with torch.no_grad():
        for i in range(size):
            z = torch.normal(mean=0,std=1,size=(1,vae.latent_dim))
            x_tilde = vae.decode(z.to(device))
            samples[i,:,:,:] = np.transpose(x_tilde.view(3,w,h).cpu().clone().detach().numpy(),(1,2,0))*0.5+0.5

    return samples

def dislpay_generator(device, vae,n = 8, width = 6):
    import numpy as np

    w, h = vae.input_size

    vae.to(device).eval()
    grid = np.empty((w*n,h*n,3))

    with torch.no_grad():
        for i in range(n):
            for j in range(n):
                z = torch.normal(mean=0,std=1,size=(1,vae.latent_dim))
                x_tilde = vae.decode(z.to(device))
                grid[i*w:(i+1)*w,j*h:(j+1)*h,:]=np.clip(np.transpose(x_tilde.view(3,w,h).cpu().clone().detach().numpy(),(1,2,0))*0.5+0.5,0,1)
        plt.axis('off')
        plt.imshow(grid)
        plt.show()

def dislpay_encoder(device, vae, test_loader):
    vae.to(device).eval()
    with torch.no_grad():
        for data, _class in test_loader:
            data = data.to(device)
            output, mean, logvar = vae.encode(data)
            output = output.cpu().clone().detach().numpy()
            plt.scatter(output[:,0],output[:,1],c=_class)
        plt.show()
