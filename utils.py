import torchvision.utils
import torch
import torchvision

import numpy as np

import os

#save image in every case (numpy, torch from cpu or gpu)
def save_image(data, file_name : str):
    # be sure directory existe
    try:
        os.makedirs(os.path.dirname(file_name))
    except OSError:
        pass

    if type(data) == np.ndarray: # in case of numpy array
        if len(data.shape) == 4: # list of rgb image
            tensor = torch.tensor(np.transpose(data[0],(2,0,1)))
        elif len(data.shape) == 3: # rgb image
            tensor = torch.tensor(np.transpose(data,(2,0,1)))
        else:
            raise Exception('Image must have 3 or 4 dimention')
    elif type(data) == torch.Tensor: # in case of torch tensor
        tensor = data.cpu().clone().detach()
        if len(tensor.size()) == 4:
            tensor = tensor[0]
        elif len(tensor.size()) != 3:
            raise Exception('Image must have 3 or 4 dimention')
    else:
        raise Exception('Image must be type numpy.ndarray or torch.Tensor')

    torchvision.utils.save_image(tensor, file_name)

#save image in every case (numpy, torch from cpu or gpu)
def save_gray_image(data, file_name : str):
    # be sure directory existe
    try:
        os.makedirs(os.path.dirname(file_name))
    except OSError:
        pass

    if type(data) == np.ndarray: # in case of numpy array
        if len(data.shape) == 4: # list of rgb image
            tensor = torch.tensor(data[0],(2,0,1))
        elif len(data.shape) == 3: # list of rgb image
            tensor = torch.tensor(data,(2,0,1))
        elif len(data.shape) == 2: # rgb image
            tensor = torch.tensor(data)
        else:
            raise Exception('Image must have 2, 3 or 4 dimention')
    elif type(data) == torch.Tensor: # in case of torch tensor
        tensor = data.cpu().clone().detach()
        if len(tensor.size()) == 4:
            tensor = tensor[0]
        elif len(tensor.size()) != 3 and len(tensor.size()) != 2:
            raise Exception('Image must have 2, 3 or 4 dimention')
    else:
        raise Exception('Image must be type numpy.ndarray or torch.Tensor')

    if len(tensor.size()) == 3 and tensor.size()[0] != 1:
        raise Exception('Gray image must have only one channel')

    torchvision.utils.save_image(tensor, file_name)