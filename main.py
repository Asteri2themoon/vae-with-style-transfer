import argparse
import matplotlib.pyplot as plt
import numpy as np

import torch
import torch.optim as optim
import torch.nn.functional as F
import torchvision

import dataset_celeba as dataset
from early_stop import EarlyStop

import os

#get hyperparameters
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

#gpu
parser_classifier = subparsers.add_parser('gpu', help='gpu stats')
parser_classifier.set_defaults(model='gpu')

#classifier
parser_classifier = subparsers.add_parser('classifier', help='classifier command')
parser_classifier.set_defaults(model='classifier')

parser_classifier.add_argument('--batch-size', '-bs', nargs='?',type = int, default = 128)
parser_classifier.add_argument('--learning-rate', '-lr', nargs='?',type = float, default = 0.01)
parser_classifier.add_argument('--momentum', '-m', nargs='?',type = float, default = 0.5)
parser_classifier.add_argument('--log-interval', '-li', nargs='?',type = int, default = 10)
parser_classifier.add_argument('--epochs', '-e', nargs='?',type = int, default = 3)

#vae
parser_classifier = subparsers.add_parser('vae', help='vae command')
parser_classifier.set_defaults(model='vae')

parser_classifier.add_argument('--batch-size', '-bs', nargs='?',type = int, default = 128)
parser_classifier.add_argument('--learning-rate', '-lr', nargs='?',type = float, default = 0.01)
parser_classifier.add_argument('--beta', '-b', nargs='?',type = float, default = 1.0)
parser_classifier.add_argument('--log-interval', '-li', nargs='?',type = int, default = 100)
parser_classifier.add_argument('--epochs', '-e', nargs='?',type = int, default = 10)
parser_classifier.add_argument('--latent-dim', '-ld', nargs='?',type = int, default = 128)
parser_classifier.add_argument('--image-size', '-is', nargs='?',type = int, default = 128)
parser_classifier.add_argument('--style-transfer', '-st', nargs='?',type = bool, default = False)
parser_classifier.add_argument('--gpu-index', '-gi', nargs='?',type = int, default = None)
parser_classifier.add_argument('--early-stop', '-es', nargs='?',type = int, default = None)
parser_classifier.add_argument('--output', '-o', nargs='?', default = './results/vae')
parser_classifier.add_argument('--input', '-i', nargs='?', default = './results/vae')
parser_classifier.add_argument('--load', '-l', action='store_true')

#process args
args = parser.parse_args()

if args.model == 'gpu':
    n = torch.cuda.device_count()
    gpu_list = ['cuda:{}'.format(i) for i in range(n)]
    for gpu_name in gpu_list:
        print(torch.cuda.get_device_name(gpu_name))
        major,minor = torch.cuda.get_device_capability(gpu_name)
        print('\tid: {}'.format(gpu_name))
        print('\tcuda version: {}.{}'.format(major,minor))
    pass
elif args.model == 'classifier':
    from model import Classifier
    import train_classifier as classifier

    #load dataset
    train_loader = dataset.get_train_loader(args.batch_size)
    test_loader = dataset.get_test_loader(args.batch_size)

    #create model and optimizer
    model = Classifier()

    optimizer = optim.SGD(classifier.parameters(),
                        lr=args.learning_rate,
                        momentum=args.momentum)
    
    #train
    train_losses = []
    for i in range(args.epochs):
        loss = classifier.train(model,optimizer,train_loader,i,args.beta)
        test_loss, acc = classifier.test(model,test_loader,args.beta,args.log_interval)

    #plot training loss
    plt.plot(train_losses)
    plt.title('Classifier training (final accuracy: {:.1f}%)'.format(acc*100))
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.show()
    
elif args.model == 'vae':
    from model import VAE
    import train_vae as vae
    import time
    from utils import save_image

    print(args)

    cuda_gpu = 'cuda'
    if (args.gpu_index is not None) and (args.gpu_index != -1):
        cuda_gpu = 'cuda:{}'.format(args.gpu_index)
    elif args.gpu_index == -1:
        cuda_gpu = 'cpu'
    device = torch.device(cuda_gpu if torch.cuda.is_available() and args.gpu_index != -1 else 'cpu')
    print('run on: {}'.format(device))

    #load dataset
    train_loader, test_loader = dataset.get_loader(args.batch_size,size = args.image_size)

    if args.load:
        #load model
        model = vae.load(device,
                        latent_dim = args.latent_dim,
                        input_size = (args.image_size,args.image_size),
                        directory = args.input)
        #vae.test(device,model,test_loader,args.beta)
    else:
        #create model and optimizer
        model = VAE(latent_dim = args.latent_dim,
                    input_size = (args.image_size,args.image_size),
                    style_transfer = args.style_transfer)
        
        optimizer = optim.Adam(model.parameters(),
                                lr=args.learning_rate)
        
        early_stop = None
        if args.early_stop is not None:
            early_stop = EarlyStop(n = args.early_stop)
        
        #train
        train_losses = []
        for i in range(args.epochs):
            t0 = time.time()

            model.to(device).eval()
            with torch.no_grad():
                for data, _ in test_loader:
                    output, mean, logvar = model(data.to(device))
                    output = output.cpu().clone()
                    break
                n = 8
                for j in range(n):
                    original = np.clip(np.transpose(data[j,:,:,:].numpy(),(1,2,0))*0.5+0.5,0,1)
                    new = np.clip(np.transpose(output[j,:,:,:].numpy(),(1,2,0))*0.5+0.5,0,1)
                    save_image(original,os.path.join(args.output,'image/training{}/original_{}.png'.format(i,j)))
                    save_image(new,os.path.join(args.output,'image/training{}/new_{}.png'.format(i,j)))
            
            loss = vae.train(device, model,optimizer,train_loader,i,args.beta,args.log_interval,
                            style_transfer = args.style_transfer, early_stop = early_stop,
                            save_state_dir = args.output)
            print('batch {} done in {:.3f} sec'.format(i,time.time()-t0))
            train_losses.extend(loss)
            vae.test(device, model,test_loader,args.beta, style_transfer = args.style_transfer)

            if early_stop is not None and early_stop.stop_condition():
                print('early stopping')
                break
    
        #plot training loss
        plt.plot(train_losses)
        plt.title('VAE training')
        plt.xlabel('epoch')
        plt.ylabel('loss')
        plt.savefig(os.path.join(args.output,'training.svg'))
        #plt.show()


    model.to(device).eval()
    with torch.no_grad():
        for data, _ in test_loader:
            output, mean, logvar = model(data.to(device))
            output = output.cpu().clone()
            break

    n = 8
    for i in range(n):
        original = np.clip(np.transpose(data[i,:,:,:].numpy(),(1,2,0))*0.5+0.5,0,1)
        new = np.clip(np.transpose(output[i,:,:,:].numpy(),(1,2,0))*0.5+0.5,0,1)
        
        #plt.subplot(2,n,i+1)
        #plt.axis('off')
        #plt.imshow(original)
        save_image(original,os.path.join(args.output,'image/original_{}.png'.format(i)))

        #plt.subplot(2,n,i+n+1)
        #plt.axis('off')
        #plt.imshow(new)
        save_image(new,os.path.join(args.output,'image/new_{}.png'.format(i)))
    #plt.show()

    images = vae.generate_sample(device,model,size = n)
    for i in range(n):
        save_image(images[i],os.path.join(args.output,'image/test_{}.png'.format(i)))
    #display generated image from the model
    #vae.dislpay_generator(device, model)
    #vae.dislpay_encoder(device, model,test_loader)
    
else:
    raise Exception('Unkown model "{}"'.format(args.model))

