import torch
import torch.nn as nn

class WorldModel(nn.Module):
    def __init__(self, state_dim = (2048+4), action_dim = 2, hidden_dim = 512):
        super(WorldModel, self).__init__()

        self.state_dim = state_dim
        self.action_dim = action_dim
        self.hidden_dim = hidden_dim

        self.encoder = nn.Sequential(
            nn.Linear(state_dim, hidden_dim),
            #nn.PReLU(),
            #nn.Linear(hidden_dim, hidden_dim)
        )
        self.decoder = nn.Sequential(
            #nn.Linear(hidden_dim, hidden_dim),
            #nn.PReLU(),
            nn.Linear(hidden_dim, state_dim)
        )
        self.gru = nn.GRU(input_size = action_dim,
                            hidden_size = hidden_dim,
                            num_layers = 1,
                            batch_first = True)

    def forward(self, s0, a):
        [batch_size, sequence_size, _] = a.size()

        h0 = self.encoder(s0)
        
        h,_ = self.gru(a, h0.view(1, batch_size, self.hidden_dim))

        h = h.reshape(-1, self.hidden_dim)
        s = self.decoder(h)

        return s.view(batch_size, sequence_size, self.state_dim)

if __name__ == '__main__':
    import numpy as np
    from torch import Tensor

    epochs = 1000
    batch_size = 128
    split_validation = 0.05
    learning_rate = 1e-3

    #load dataset
    dataset = np.load('./files/sequences/test_10/sequences.npz')
    sparse_graphics = dataset.f.sparse_graphics
    sensors = dataset.f.sensors
    actions = dataset.f.actions

    #value clipping 
    sensors[:,:,0] *= 1/50
    sensors[:,:,2] *= 1/10000
    sensors[:,:,3] *= 1/4

    # split training and validation data
    length = sparse_graphics.shape[0]
    split = int(length*split_validation)

    val_sparse_graphics = sparse_graphics[-split:]
    sparse_graphics = sparse_graphics[:-split]
    val_sensors = sensors[-split:]
    sensors = sensors[:-split]
    val_actions = actions[-split:]
    actions = actions[:-split]

    # create model and optimizer
    wm = WorldModel()
    wm = wm.cuda()

    optimizer = torch.optim.Adam(wm.parameters(), lr=learning_rate, weight_decay = 1e-5)
    mse = nn.MSELoss()

    seq_len = sparse_graphics.shape[1]
    batch_count = sparse_graphics.shape[0]//batch_size
    val_batch_count = val_sparse_graphics.shape[0]//batch_size

    #training
    for e in range(epochs):
        train_loss, val_loss = 0, 0

        wm.train()
        for i in range(batch_count):
            print('{:.1f}%'.format((i+1)*100.0/batch_count),end='\r')

            optimizer.zero_grad()

            # prepare data
            offset = i*batch_size
            s0 = Tensor(np.concatenate((sparse_graphics[offset:offset+batch_size,0],
                                sensors[offset:offset+batch_size,0]),
                                axis = 1)).cuda()
            s = Tensor(np.concatenate((sparse_graphics[offset:offset+batch_size,1:],
                                sensors[offset:offset+batch_size,1:]),
                                axis = 2)).cuda()
            a = Tensor(actions[offset:offset+batch_size,:-1,:]).cuda()
            
            # calculate loss
            s_tilde = wm.forward(s0, a)
            loss = mse(s_tilde, s)
            train_loss += loss.item()

            # update
            loss.backward()
            optimizer.step()
        train_loss /= batch_count

        wm.eval()
        for i in range(val_batch_count):
            # prepare data
            offset = i*batch_size
            s0 = Tensor(np.concatenate((val_sparse_graphics[offset:offset+batch_size,0],
                                val_sensors[offset:offset+batch_size,0]),
                                axis = 1)).cuda()
            s = Tensor(np.concatenate((val_sparse_graphics[offset:offset+batch_size,1:],
                                val_sensors[offset:offset+batch_size,1:]),
                                axis = 2)).cuda()
            a = Tensor(val_actions[offset:offset+batch_size,:-1,:]).cuda()
            
            # calculate loss
            s_tilde = wm.forward(s0, a)
            loss = mse(s_tilde, s)

            val_loss += loss.item()
        val_loss /= val_batch_count

        print('epoch: {} training: {:.3f} validation: {:.3f}'.format(e, train_loss, val_loss))
        torch.save(wm.state_dict(), 'wm.pth')

    
