import torch
from train_style_transfer import StyleLoss,StyleLoss2

N = 8
width = 32
features = 20

A = torch.distributions.Normal(0,1).sample((N,features,width,width))

style1 = []
for i in range(N):
    tmp = StyleLoss.gram_matrix(A[i,:,:,:].view(1,features,width,width))
    print(tmp.size())
    style1.append(tmp.view(1,features,features))

style1 = torch.cat(style1)
print(style1.size())

style2 = StyleLoss2.gram_matrix(A)
print(style2.size())

print(style1.norm())
print(style2.norm())
print((style1-style2).norm())

