import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np

from vaegan import VAEGAN

def rolling_average(x, gamma):
    res = np.empty(len(x))
    res[0] = x[0]
    for i,val in enumerate(x[1:]):
        res[i+1] = res[i]*(1-gamma) + val*gamma
    return res

def save_losses(file_name,l_enc, l_dec, l_dis, l_prior, l_content, l_gan, l_gan_dec,parameters):
    import matplotlib.pyplot as plt

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    fig = plt.gcf()
    fig.suptitle('VAE/GAN training\nenc lr:{enc_lr}, dec lr:{dec_lr}, dis lr:{dis_lr}, gamma:{gamma}, latent dim:{latent_dim}'.format(**parameters), fontsize=12)

    plt.subplot(121)
    plt.xlabel('batch')
    plt.ylabel('loss')
    plt.yscale('log')
    plt.plot(rolling_average(l_enc,0.1), c=colors[0])
    plt.plot(rolling_average(l_dec,0.1), c=colors[1])
    plt.plot(rolling_average(l_dis,0.1), c=colors[2])
    plt.plot(l_enc, c=colors[0], alpha=0.5)
    plt.plot(l_dec, c=colors[1], alpha=0.5)
    plt.plot(l_dis, c=colors[2], alpha=0.5)
    plt.legend(['Lencoder','Ldecoder','Ldiscriminator'])
    plt.subplot(122)
    plt.xlabel('batch')
    plt.ylabel('loss')
    plt.yscale('log')
    plt.plot(rolling_average(l_prior,0.1), c=colors[0])
    plt.plot(rolling_average(l_content,0.1), c=colors[1])
    plt.plot(rolling_average(l_gan,0.1), c=colors[2])
    plt.plot(rolling_average(l_gan_dec,0.1), c=colors[3])
    plt.plot(l_prior, c=colors[0], alpha=0.5)
    plt.plot(l_content, c=colors[1], alpha=0.5)
    plt.plot(l_gan, c=colors[2], alpha=0.5)
    plt.plot(l_gan_dec, c=colors[3], alpha=0.5)
    plt.legend(['Lprior','Lcontent','Lgan (dis)','Lgan (dec)'])
    
    plt.savefig(file_name)
    plt.close()

def train(directory, device, train_loader, test_loader, parameters):
    import time
    from dataset_celeba import get_loader
    import matplotlib.pyplot as plt
    import os
    from utils import save_image

    try:
        os.makedirs(directory)
    except OSError:
        pass

    #batch_size = 64
    #train_loader, test_loader = get_loader(batch_size = batch_size, size = 64)
    batch_count = len(train_loader)
    
    model = VAEGAN(input_size = (64, 64),latent_dim = parameters['latent_dim']).to(device)

    optimizer_enc = torch.optim.RMSprop(model.encoder.parameters(), lr=parameters['enc_lr'], weight_decay = 1e-5)
    optimizer_dec = torch.optim.Adam(model.decoder.parameters(), lr=parameters['dec_lr'], weight_decay = 1e-5)
    optimizer_dis = torch.optim.RMSprop(model.discriminator.parameters(), lr=parameters['dis_lr'], weight_decay = 1e-5)

    losses_enc, losses_dec, losses_dis = [], [], []
    losses_prior, losses_content, losses_gan, losses_gan_dec = [], [], [], []

    for i in range(parameters['epochs']):
        for j,(x,_) in enumerate(train_loader):
            t0 = time.time()
            # init
            model.train()
            optimizer_enc.zero_grad()
            optimizer_dec.zero_grad()
            optimizer_dis.zero_grad()

            # calculate losses
            loss_enc, loss_dec, loss_dis, l_prior, l_content, l_gan, l_gan_dec = model.loss_function(x.to(device),
                                                                                    beta = parameters['beta'],
                                                                                    gamma = parameters['gamma'],
                                                                                    device = device)

            # update weights
            loss_enc.backward(retain_graph = True)
            loss_dec.backward(retain_graph = True)
            loss_dis.backward(retain_graph = False)

            optimizer_enc.step()
            optimizer_dec.step()
            optimizer_dis.step()

            # logs
            losses_enc.append(loss_enc.item())
            losses_dec.append(loss_dec.item())
            losses_dis.append(loss_dis.item())

            losses_prior.append(l_prior.item())
            losses_content.append(l_content.item())
            losses_gan.append(l_gan.item())
            losses_gan_dec.append(l_gan_dec.item())

            print('epoch {} batch {}/{} loss: enc {:.4f} dec {:.4f} dis {:.4f} prior {:.4f} content {:.4f} gan {:.4f}/{:.4f} ({:.3f} sec)'.format(i,j,batch_count,losses_enc[-1],losses_dec[-1],losses_dis[-1],losses_prior[-1],losses_content[-1],losses_gan[-1],losses_gan_dec[-1],time.time()-t0))
            #print('{} {:.3f} sec'.format(device,time.time()-t0))
            if (j%100)==0:
                save_losses(os.path.join(directory,'training.svg'),losses_enc,losses_dec,losses_dis,losses_prior,losses_content,losses_gan,losses_gan_dec,parameters)
        save_losses(os.path.join(directory,'training.svg'),losses_enc,losses_dec,losses_dis,losses_prior,losses_content,losses_gan,losses_gan_dec,parameters)
        torch.save(model.state_dict(), os.path.join(directory,'vaegan.pth'))
        
        # display test
        for x,_ in test_loader:
            break
    
        x = x.to(device)
        z,_,_ = model.encode(x)
        x_tilde = model.decode(z)

        x = x.cpu().detach().clone()*0.5+0.5
        x_tilde = x_tilde.cpu().detach().clone()*0.5+0.5

        N = 2
        for k in range(N):
            plt.subplot(2,N,k+1)
            plt.axis('off')
            save_image(x[k,:,:,:],os.path.join(directory,'results/epoch{}/x{}.png'.format(i,k)))
            plt.imshow(np.transpose(x[k,:,:,:].numpy(),(1,2,0)))
            plt.subplot(2,N,k+N+1)
            plt.axis('off')
            save_image(x_tilde[k,:,:,:],os.path.join(directory,'results/epoch{}/x_tilde{}.png'.format(i,k)))
            plt.imshow(np.transpose(x_tilde[k,:,:,:].numpy(),(1,2,0)))
        plt.savefig(os.path.join(directory,'test{}.png'.format(i)))
        #plt.show()
        plt.close()

    del optimizer_enc, optimizer_dec, optimizer_dis
    del model

    torch.cuda.empty_cache()

    return np.transpose(x[0,:,:,:].numpy(),(1,2,0))

if __name__ == '__main__':
    import threading
    from dataset_celeba import get_loader
    import matplotlib.pyplot as plt

    batch_size = 4
    train_loader, test_loader = get_loader(batch_size = batch_size, size = 64, directory = './files/celeba')

    parameters = [{'enc_lr':3e-7,'dec_lr':3e-5,'dis_lr':1e-6,'gamma':1e-3,'beta':4,'epochs':1000,'latent_dim':256}]
    
    def par2dir(parameter):
        dir_name = 'final-{}-{}-{}-{}-{}-{}'.format(str(parameter['enc_lr']).replace('.','_'),
                                                    str(parameter['dec_lr']).replace('.','_'),
                                                    str(parameter['dis_lr']).replace('.','_'),
                                                    str(parameter['gamma']).replace('.','_'),
                                                    str(parameter['beta']).replace('.','_'),
                                                    str(parameter['latent_dim']).replace('.','_'))
        return dir_name

    res = []
    for p in parameters:
        success = False
        while not success:
            try:
                img_test = train(par2dir(p),'cuda:0', train_loader, test_loader, p)
                success = True
            except KeyboardInterrupt:
                exit(0)
            except Exception as e:
                print('error {}, restart test {}'.format(str(e),p))
        res.append(res)
    

