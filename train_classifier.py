from model import Classifier

import numpy as np

import os

import torch
import torch.optim as optim
import torch.nn.functional as F

def train(classifier,optimizer,train_loader,epoch, log_interval = 10, save_state_dir = './results/classifier'):
    train_class_losses = []
    classifier.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = classifier(data)
        loss = F.cross_entropy(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
            epoch, batch_idx * len(data), len(train_loader.dataset),
            100. * batch_idx / len(train_loader), loss.item()))
            train_class_losses.append(loss.item())
            torch.save(classifier.state_dict(), os.path.join(save_state_dir,'model.pth'))
            torch.save(optimizer.state_dict(), os.path.join(save_state_dir,'optimizer.pth'))
    return train_class_losses

def test(classifier,test_loader):
    classifier.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            output = classifier(data)
            test_loss = F.cross_entropy(output, target, size_average=False).item()
            pred = output.data.max(1, keepdim=True)[1]
            correct += pred.eq(target.data.view_as(pred)).sum()
        test_loss /= len(test_loader.dataset)
        print('\nTest set: Avg. loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return test_loss, correct / len(test_loader.dataset)