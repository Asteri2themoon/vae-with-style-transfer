import torch
import numpy as np

class EarlyStop:
    def __init__(self, n : int):
        assert n > 0

        self.n = n
        self.losses = []

    def add_batch_loss(self, losses):
        assert type(losses) == list or type(losses) == float or (type(losses) == torch.Tensor and 0 <= len(losses.size()) <= 1)

        if type(losses) == float:
            losses = [losses]
        elif type(losses) == torch.Tensor:
            losses = losses.view(-1)
            m = losses.size()[0]
            losses = [losses[i].item() for i in range(m)]

        tmp = self.losses + losses
        self.losses = tmp[-self.n:]

    def stop_condition(self):
        if len(self.losses) >= self.n:
            # is all of the the n-1 last value are greater than the n last value?
            return np.argmin(self.losses) == 0
        else:
            return False
