import torch
import torch.nn as nn
import torch.nn.functional as F

import json
import os

from train_style_transfer import CnnStyleLoss

class Classifier(nn.Module):
    def __init__(self):
        super(Classifier,self).__init__()

        self.classifier = nn.Sequential(
            nn.Conv2d(1,32,3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,32,3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,64,3, stride = 2, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64,64,3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64,128,3, stride = 2, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128,128,3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(16*16*128,10),
            nn.Softmax(1)
        )
    
    def forward(self,x):
        return self.classifier(x)

class VAE(nn.Module):
    def __init__(self, latent_dim = 128, input_size = (128,128), style_transfer = False):
        super(VAE,self).__init__()

        self.input_size = input_size

        self.latent_dim = latent_dim

        self.encoder = nn.Sequential(
            nn.Conv2d(3,32,3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,32,3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32,64,3, stride = 2, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64,64,3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64,128,3, stride = 2, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128,128,3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
        )

        w, h = self.input_size

        self.fc_mean = nn.Linear((w//4)*(h//4)*128, latent_dim)
        self.fc_logvar = nn.Linear((w//4)*(h//4)*128, latent_dim)

        self.fc_z = nn.Linear(latent_dim, (w//4)*(h//4)*128)

        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(128,128,3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.ConvTranspose2d(128,64,3, stride = 2, padding = 1, output_padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.ConvTranspose2d(64,64,3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.ConvTranspose2d(64,32,3, stride = 2, padding = 1, output_padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.ConvTranspose2d(32,32,3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.ConvTranspose2d(32,3,3, padding = 1),
            nn.Tanh()
        )

        self.style_transfer = style_transfer
        if style_transfer:
            self.style_transfer_loss = CnnStyleLoss()

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std
    
    def encode(self, x):
        w, h = self.input_size
        encoded = self.encoder(x).view(-1, (w//4)*(h//4)*128)
        mean = self.fc_mean(encoded)
        logvar = self.fc_logvar(encoded)
        
        z = self.reparameterize(mean, logvar)

        return z, mean, logvar

    def decode(self, z):
        w, h = self.input_size
        x_tilde = self.decoder(self.fc_z(z).view(-1, 128, w//4, h//4))

        return x_tilde

    def forward(self,x):
        z, mean, logvar = self.encode(x)
        x_tilde = self.decode(z)

        return x_tilde, mean, logvar
    
    def loss_function(self, x, x_tilde, mu, logvar, beta):
        w, h = self.input_size
        BCE = F.mse_loss(x_tilde.view(-1, w*h*3), x.view(-1, w*h*3), reduction='mean')

        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())
        
        return BCE + beta*KLD, BCE, KLD
    
    def loss_function_with_transfer(self, x, x_tilde, mu, logvar, beta,
                                    style_weight=0.001, content_weight=0.01):
        assert self.style_transfer

        w, h = self.input_size

        BCE = F.mse_loss(x_tilde.view(-1, w*h*3), x.view(-1, w*h*3), reduction='mean')

        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp())

        KLD *= beta

        # style transfer loss
        target = x*0.5+0.5
        style_loss, content_loss = self.style_transfer_loss(x_tilde*0.5+0.5, target, target,
                                                    gram_layer = [0,1,2,3,4],
                                                    content_layer = [3])
        

        style_loss *= style_weight
        content_loss *= content_weight
        
        return style_loss + BCE + KLD, style_loss, BCE, KLD
    
    def save(self, directory : str):
        parameters = {
            'input_size': self.input_size,
            'latent_dim': self.latent_dim
        }
        with open(os.path.join(directory,'parameters.json'), 'w') as f:
            json.dump(parameters, f)

