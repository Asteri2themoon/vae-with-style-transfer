import torch
import torchvision

def get_train_loader(device, batch_size,directory = './files/'):
    return torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(directory, train=True, download=True,
                                    transform=torchvision.transforms.Compose([
                                    torchvision.transforms.ToTensor(),
                                    torchvision.transforms.Normalize(
                                        (0.1307,), (0.3081,))
                                    ])),
        batch_size=batch_size, shuffle=True).to(device)

def get_test_loader(device, batch_size,directory = './files/'):
    return torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(directory, train=False, download=True,
                                    transform=torchvision.transforms.Compose([
                                    torchvision.transforms.ToTensor(),
                                    torchvision.transforms.Normalize(
                                        (0.1307,), (0.3081,))
                                    ])),
        batch_size=batch_size, shuffle=True).to(device)